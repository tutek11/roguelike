package cz.cuni.gamedev.nail123.roguelike.world.worlds

import cz.cuni.gamedev.nail123.roguelike.blocks.Floor
import cz.cuni.gamedev.nail123.roguelike.blocks.GameBlock
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Enemy
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Rat
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Snake
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Door
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Pot
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Stairs
import cz.cuni.gamedev.nail123.roguelike.entities.unplacable.FogOfWar
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import cz.cuni.gamedev.nail123.roguelike.extensions.chebyshevDistance
import cz.cuni.gamedev.nail123.roguelike.extensions.euclideanDistance
import cz.cuni.gamedev.nail123.roguelike.world.Area
import cz.cuni.gamedev.nail123.roguelike.world.World
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder
import cz.cuni.gamedev.nail123.roguelike.world.builders.EmptyAreaBuilder
import org.hexworks.zircon.api.data.Position3D
import org.hexworks.zircon.api.data.Size3D
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.random.Random
import kotlin.reflect.full.createInstance

/**
 * Sample world, made as a starting point for creating custom worlds.
 * It consists of separate levels - each one has one staircase, and it leads infinitely deep.
 */
class AgentWorld: World() {
    // Constants
    companion object
    {
        // A single room appearance
        val MIN_ROOM_SIZE : Size3D = Size3D.create(4,4,1)
        val MAX_ROOM_SIZE : Size3D = Size3D.create(10,10,1)
        val MAX_ROOM_DIST : Position3D = Position3D.create(12,12,1)
        val ROOM_PILLAR_CHANCE : Float = 0.33f
        val MIN_POTS_IN_ROOM : Int = 2
        val MAX_POTS_IN_ROOM : Int = 15

        // All room generation
        val MAX_ROOM_GEN_TRIES : Int = 20000
        val LEVEL_1_NUMBER_OF_ROOMS : Int = 4
        val LEVEL_NUMBER_OF_ROOMS_MULT : Float = 1.5f
        val PERCENT_OF_ROOMS_WITHOUT_ENEMIES : Float = 0.25f // First pass
        val START_SNAKE_CHANCE : Float = 0.2f
        val ADD_SNAKE_CHANCE : Float = 0.05f
        val MAX_SNAKE_CHANCE : Float = 0.8f

        // Corridors
        val MIN_CORR_SEGMENT_LEN : Int = 1
        val MAX_CORR_SEGMENT_LEN : Int = 3
        val CORR_SEGMENT_RANDOM_CHANCE : Float = 0.05f
        val CORR_TRIES : Int = 20000
        val CORR_ENEMY_CHANCE : Float = 0.01f

        // Stairs
        val TOP_ROOM_PROCENT : Float = 0.75f
    }

    var currentLevel = 0

    override fun buildStartingArea() = buildLevel()

    /**
     * Builds one of the levels.
     */
    fun buildLevel(): Area {
        // Start with an empty area
        val areaBuilder = EmptyAreaBuilder().create()

        // Place the player at a random empty location
        areaBuilder.addAtEmptyPosition(
                areaBuilder.player,
                Position3D.create(5, 5, 0),
                Size3D.create(areaBuilder.width - 8, areaBuilder.height - 8, 1)
        )
        val playerPos = areaBuilder.player.position

        // Fill the map to walls for the digger
        fillWholeMap(areaBuilder, Wall())

        // Generate the first room
        var room1 = placeRoom(areaBuilder, playerPos)
        while (!room1.isValid) placeRoom(areaBuilder, playerPos)

        // Setup lists of generated structures
        val allRooms : MutableList<Room> = mutableListOf(room1)
        val allCorridors : MutableList<Corridor> = mutableListOf()

        // Set dummy room (basically null)
        var room2 = Room(false, Position3D.unknown(), Size3D.one())

        // Initialize the infinite loop prevention counter
        var currGenStopTries = 0

        var currRoomNum = 1
        val maxRoomNum = LEVEL_1_NUMBER_OF_ROOMS + currentLevel * LEVEL_NUMBER_OF_ROOMS_MULT
        while (currRoomNum < maxRoomNum)
        {

            // Try to generate a valid room to connect to an existing room
            room2.isValid = false
            while (!room2.isValid)
            {
                room1 = allRooms.random()

                // To prevent being stuck in infinite loop stop the gen after a few tries
                currGenStopTries += 1
                if (currGenStopTries >= MAX_ROOM_GEN_TRIES)
                {
                    break
                }

                val pos = Position3D.create(room1.position.x + Random.nextInt(-MAX_ROOM_DIST.x, MAX_ROOM_DIST.x),
                                            room1.position.y + Random.nextInt(-MAX_ROOM_DIST.y, MAX_ROOM_DIST.y), 0)
                if (pos.x < MIN_ROOM_SIZE.xLength || pos.x >= areaBuilder.width - MIN_ROOM_SIZE.xLength ||
                    pos.y < MIN_ROOM_SIZE.yLength ||  pos.y >= areaBuilder.height - MIN_ROOM_SIZE.yLength) continue

                room2 = placeRoom(areaBuilder, pos)
            }

            // Infinite loop break
            if (currGenStopTries >= MAX_ROOM_GEN_TRIES)
            {
                break
            }

            // Generate the connecting points and corridor
            val connectingPoints = getConnectingPoints(areaBuilder, room1, room2)
            val corr = placeCorridor(areaBuilder, connectingPoints.first, connectingPoints.second)

            // In case of invalid corridor, remove the room and try the whole thing again
            if (!corr.isValid)
            {
                removeRoom(areaBuilder, room2)
                continue
            }

            // Save generated structures
            allRooms += room2
            allCorridors += corr

            // Upon successfully generating a room, reset the fail counter
            currGenStopTries = 0
            currRoomNum += 1
        }

        println(allRooms.size.toString() + " rooms have been generated!")

        // Place level elements
        placeStairs(areaBuilder, allRooms, playerPos)
        placeEnemies(areaBuilder, allRooms, allCorridors)
        placePots(areaBuilder, allRooms)

        // Place floor under player, just to be sure
        areaBuilder.blocks[playerPos] = Floor()
        areaBuilder.addEntity(areaBuilder.player, playerPos)

        // We add fog of war such that exploration is needed
        areaBuilder.addEntity(FogOfWar(), Position3D.unknown())

        // Build it into a full Area
        return areaBuilder.build()
    }


    /**
     * Fills the whole map with given block
     */
    private fun fillWholeMap(areaBuilder: AreaBuilder, block : GameBlock)
    {
        for (x in 0..<areaBuilder.width)
        {
            for (y in 0..<areaBuilder.height)
            {
                val pos = Position3D.create(x, y, 0)

                areaBuilder.blocks[pos] = block::class.createInstance()
            }
        }
    }


    /**
     * Places a room at given position of a given size. Also checks not to override existing terrain
     */
    private fun placeRoom(areaBuilder: AreaBuilder, topLeftCorner : Position3D) : Room
    {
        // Generate a random size
        val size = Size3D.create(Random.nextInt(MIN_ROOM_SIZE.xLength, MAX_ROOM_SIZE.xLength + 1),
                                 Random.nextInt(MIN_ROOM_SIZE.yLength, MAX_ROOM_SIZE.yLength + 1),1)

        // Check the x bounds
        var xStart : Int = topLeftCorner.x
        if (xStart < 1) xStart = 1
        var xEnd : Int = xStart + size.xLength
        if (xEnd >= areaBuilder.width) xEnd = areaBuilder.width - 1

        // Check the y bounds
        var yStart : Int = topLeftCorner.y
        if (yStart <= 0) yStart = 1
        var yEnd : Int = yStart + size.yLength
        if (yEnd >= areaBuilder.height) yEnd = areaBuilder.height - 1


        // Check if area is clear
        for (x in xStart - 2 .. xEnd + 2){
            for (y in yStart - 2 .. yEnd + 2){
                if (areaBuilder.blocks[Position3D.create(x, y, 0)] is Floor) {
                    return Room(false, Position3D.unknown(), Size3D.one())
                }
            }
        }

        // Place the room
        for (x in xStart..<xEnd){
            for (y in yStart..<yEnd){
                areaBuilder.blocks[Position3D.create(x, y, 0)] = Floor()
            }
        }

        // Place pillars in a room
        var pillarCount = 0

        // If a room is big enough, and we are lucky generate appropriate amount of pillars
        if (Random.nextFloat() < ROOM_PILLAR_CHANCE)
        {
            pillarCount = if (size.xLength < 5 && size.yLength < 5) 0 else if (max(size.xLength, size.yLength) < 8) 1 else 2
        }

        // Place a single pillar in the middle of the room
        if (pillarCount == 1){
            val pillarPos = topLeftCorner + Position3D.create(size.xLength/2 - 1, size.yLength/2 - 1, 0)
            areaBuilder.blocks[pillarPos] = Wall()
            areaBuilder.blocks[pillarPos + Position3D.create(1,0,0)] = Wall()
            areaBuilder.blocks[pillarPos + Position3D.create(0,1,0)] = Wall()
            areaBuilder.blocks[pillarPos + Position3D.create(1,1,0)] = Wall()

        }
        else if (pillarCount >= 2)
        {
            var pillarPos : Position3D
            if (size.xLength > size.yLength)
            {
                pillarPos = topLeftCorner + Position3D.create(2, size.yLength/2 - 1, 0)
            }
            else
            {
                pillarPos = topLeftCorner + Position3D.create(size.xLength/2 - 1, 2, 0)
            }
            areaBuilder.blocks[pillarPos] = Wall()
            areaBuilder.blocks[pillarPos + Position3D.create(1,0,0)] = Wall()
            areaBuilder.blocks[pillarPos + Position3D.create(0,1,0)] = Wall()
            areaBuilder.blocks[pillarPos + Position3D.create(1,1,0)] = Wall()

            if (size.xLength > size.yLength)
            {
                pillarPos = topLeftCorner + Position3D.create(size.xLength - 4, size.yLength/2 - 1 , 0)
            }
            else
            {
                pillarPos = topLeftCorner + Position3D.create(size.xLength/2 - 1, size.yLength - 4, 0)
            }

            areaBuilder.blocks[pillarPos] = Wall()
            areaBuilder.blocks[pillarPos + Position3D.create(1,0,0)] = Wall()
            areaBuilder.blocks[pillarPos + Position3D.create(0,1,0)] = Wall()
            areaBuilder.blocks[pillarPos + Position3D.create(1,1,0)] = Wall()
        }

        return Room(true, Position3D.create(xStart, yStart, 0), size)
    }


    /**
     * Removes a placed room
     */
    private fun removeRoom(areaBuilder: AreaBuilder, room: Room)
    {
        // Check the x bounds
        var xStart : Int = room.position.x
        if (xStart < 1) xStart = 1
        var xEnd : Int = xStart + room.size.xLength
        if (xEnd >= areaBuilder.width) xEnd = areaBuilder.width - 1

        // Check the y bounds
        var yStart : Int = room.position.y
        if (yStart <= 0) yStart = 1
        var yEnd : Int = yStart + room.size.yLength
        if (yEnd >= areaBuilder.height) yEnd = areaBuilder.height - 1

        // remove the room
        for (x in xStart..<xEnd){
            for (y in yStart..<yEnd){
                areaBuilder.blocks[Position3D.create(x, y, 0)] = Wall()
            }
        }
    }


    /**
     * Places the stairs to the next level based on the room list structure.
     */
    private fun placeStairs(areaBuilder: AreaBuilder, allRooms : List<Room>, playerPos : Position3D)
    {
        // Get the top most (last added -> further away from the player) percent of rooms
        val randBottomIdx = (allRooms.size * TOP_ROOM_PROCENT).toInt()

        // Find the room furthest from the player euclidean distance vise
        var maxDist = Double.MIN_VALUE
        var bestRoom = allRooms.random()
        for (idx in randBottomIdx..<allRooms.size)
        {
            val distance = (allRooms[idx].position - playerPos).euclideanDistance
            if (distance > maxDist)
            {
                maxDist = distance
                bestRoom = allRooms[idx]
            }
        }

        // Get a tile in a room without a wall tile
        var posInRoom = bestRoom.position
        while (areaBuilder.blocks[posInRoom] is Wall) {
            posInRoom = Position3D.create(
                bestRoom.position.x + Random.nextInt(1, bestRoom.size.xLength - 1),
                bestRoom.position.y + Random.nextInt(1, bestRoom.size.yLength - 1), 0
            )
            if (posInRoom.x < 1) posInRoom = posInRoom.withX(1)
            if (posInRoom.x >= areaBuilder.width - 1) posInRoom = posInRoom.withX(areaBuilder.width - 2)
            if (posInRoom.y < 1) posInRoom = posInRoom.withY(1)
            if (posInRoom.y >= areaBuilder.height - 1) posInRoom = posInRoom.withY(areaBuilder.height - 2)
        }

        // Place a floor just to be sure and stairs
        areaBuilder.blocks[posInRoom] = Floor()
        areaBuilder.addEntity(Stairs(), posInRoom)
    }


    /**
     * Places a randomly sized corridor.
     */
    private fun placeCorridor(areaBuilder: AreaBuilder, startPos: Position3D, endPos: Position3D) : Corridor
    {
        var path : MutableList<Position3D> = mutableListOf(startPos)
        var currPos = startPos
        var numTotalTries = 0

        // While a valid path has not been found repeat
        while (currPos != endPos && numTotalTries < CORR_TRIES)
        {
            path = mutableListOf(startPos)
            currPos = startPos
            var dirToGoal = getMostProminentDirection(endPos - currPos)
            var currDir = if (Random.nextFloat() > CORR_SEGMENT_RANDOM_CHANCE) dirToGoal else Position3D.create(dirToGoal.y, dirToGoal.x, 0)
            var tangent = Position3D.create(currDir.y, currDir.x, 0)
            var turnCountdown = Random.nextInt(MIN_CORR_SEGMENT_LEN, MAX_CORR_SEGMENT_LEN + 1)
            var maxBackTrackTries = 1

            // While the path is valid, go further
            while (currPos != endPos && numTotalTries < CORR_TRIES)
            {
                // We change direction after a few steps
                if (turnCountdown == 0)
                {
                    dirToGoal = getMostProminentDirection(endPos - currPos)
                    if (Random.nextFloat() > CORR_SEGMENT_RANDOM_CHANCE)
                    {
                        currDir = dirToGoal
                    }
                    else
                    {
                        if (Random.nextBoolean()) currDir = Position3D.create(dirToGoal.y, dirToGoal.x, 0)
                        else currDir = Position3D.create(-dirToGoal.y, -dirToGoal.x, 0)
                    }
                    tangent = Position3D.create(currDir.y, currDir.x, 0)
                    turnCountdown = Random.nextInt(MIN_CORR_SEGMENT_LEN, MAX_CORR_SEGMENT_LEN + 1)
                }

                // Check around the path not to override terrain
                val newPos = currPos + currDir
                if (areaBuilder.blocks[newPos] is Floor ||
                    //(newPos + currDir != endPos && areaBuilder.blocks[newPos + currDir] is Floor) ||
                    areaBuilder.blocks[newPos + tangent] is Floor ||
                    areaBuilder.blocks[newPos - tangent] is Floor ||
                    areaBuilder.blocks[newPos + tangent + tangent] is Floor ||
                    areaBuilder.blocks[newPos - tangent - tangent] is Floor ||
                    (newPos != endPos && newPos + currDir != endPos && areaBuilder.blocks[newPos + tangent + currDir] is Floor) ||
                    (newPos != endPos && newPos + currDir != endPos && areaBuilder.blocks[newPos - tangent + currDir] is Floor) ||
                    (newPos != endPos && newPos + currDir != endPos && areaBuilder.blocks[newPos + tangent + tangent + currDir + currDir] is Floor) ||
                    (newPos != endPos && newPos + currDir != endPos && areaBuilder.blocks[newPos - tangent - tangent + currDir + currDir] is Floor) ||
                    newPos.x < 1 || newPos.x >= areaBuilder.width - 1 ||
                    newPos.y < 1 || newPos.y >= areaBuilder.height - 1 ||
                    path.contains(newPos))
                {
                    // Backtrack based on maxTries (which is the number of times the agent failed)
                    var numBacktrackTries = maxBackTrackTries
                    while (numBacktrackTries != 0)
                    {
                        if (path.size == 1)
                        {
                            currPos = startPos
                        }
                        else
                        {
                            path.removeLast()
                            currPos = path.last()
                        }
                        numBacktrackTries -= 1
                    }
                    maxBackTrackTries = min(maxBackTrackTries + 1, path.size)
                    numTotalTries += 1
                    turnCountdown = 0
                    continue
                }

                // Valid position found -> added to path
                currPos += currDir
                turnCountdown -= 1
                path += currPos
            }
        }

        if (currPos != endPos)
        {
            return Corridor(false, path)
        }

        // Place the generated corridor
        for (pos in path)
        {
            areaBuilder.blocks[pos] = Floor()
        }

        // Place doors
        areaBuilder.blocks[startPos] = Floor()
        areaBuilder.addEntity(Door(), startPos)
        areaBuilder.blocks[endPos] = Floor()
        areaBuilder.addEntity(Door(), endPos)

        return Corridor(true, path)
    }


    /**
     * Return a vector with the most prominent direction in the given Position3D
     */
    private fun getMostProminentDirection(position: Position3D) : Position3D
    {
        if (abs(position.x) > abs(position.y))
        {
            return if (position.x > 0) Position3D.create(1,0,0) else Position3D.create(-1,0,0)
        }
        else
        {
            return if (position.y > 0) Position3D.create(0,1,0) else Position3D.create(0,-1,0)
        }
    }


    /**
     * Returns a pair of Position3D on the room edge which becomes the connecting point for a corridor
     * First of the pair is connecting point in room1 and second is the connecting point in room2
     */
    private fun getConnectingPoints(areaBuilder: AreaBuilder, room1 : Room, room2 : Room) : Pair<Position3D, Position3D>
    {
        val room1Width = room1.size.xLength
        val room1Height = room1.size.yLength
        val room1Center = room1.position + Position3D.create(room1Width/2, room1Height/2,0)
        val room2Width = room2.size.xLength
        val room2Height = room2.size.yLength
        val room2Center = room2.position + Position3D.create(room2Width/2, room2Height/2,0)

        val directionFromR1ToR2 = room2Center - room1Center
        val possibleDirectionRoom1 = Position3D.create(if (directionFromR1ToR2.x > 0) room1Width else -1,
                                                       if (directionFromR1ToR2.y > 0) room1Height else -1, 0)
        val possibleDirectionRoom2 = Position3D.create(if (directionFromR1ToR2.x < 0) room2Width else -1,
                                                       if (directionFromR1ToR2.y < 0) room2Height else -1, 0)

        // Generate the start point
        var startPoint : Position3D
        if (Random.nextBoolean())
        {
            val edgePos = room1.position + possibleDirectionRoom1.withY(0)
            startPoint = edgePos + Position3D.create(0, Random.nextInt( 1, room1Height),0)
        }
        else
        {
            val edgePos = room1.position + possibleDirectionRoom1.withX(0)
            startPoint = edgePos + Position3D.create(Random.nextInt(1, room1Width),0 ,0)
        }

        // Check X bounds of start point
        if (startPoint.x <= 0) startPoint = startPoint.withX(1)
        if (startPoint.x >= areaBuilder.width - 1) startPoint = startPoint.withX(areaBuilder.width - 2)

        // Check Y bounds of start point
        if (startPoint.y <= 0) startPoint = startPoint.withY(1)
        if (startPoint.y >= areaBuilder.height - 1) startPoint = startPoint.withY(areaBuilder.height - 2)


        // Generate the end point
        var endPoint : Position3D
        if (Random.nextBoolean())
        {
            val edgePos = room2.position + possibleDirectionRoom2.withY(0)
            endPoint = edgePos + Position3D.create(0, Random.nextInt(1, room2Height) ,0)
        }
        else
        {
            val edgePos = room2.position +possibleDirectionRoom2.withX(0)
            endPoint = edgePos + Position3D.create(Random.nextInt(1, room2Width),0 ,0)
        }

        // Check X bounds of end point
        if (endPoint.x <= 0) endPoint = endPoint.withX(1)
        if (endPoint.x >= areaBuilder.width - 1) endPoint = endPoint.withX(areaBuilder.width - 2)

        // Check Y bounds of end point
        if (endPoint.y <= 0) endPoint = endPoint.withY(1)
        if (endPoint.y >= areaBuilder.height - 1) endPoint = endPoint.withY(areaBuilder.height - 2)


        return Pair(startPoint, endPoint)
    }


    /**
     * Places enemies in rooms based on the number of rooms a current level
     */
    private fun placeEnemies(areaBuilder: AreaBuilder, allRooms: MutableList<Room>, allCorridors: MutableList<Corridor>) {
        val maxNumEnemies = allRooms.size * 1.5f
        var currNumEnemies = 0

        for (room in allRooms)
        {
            if (Random.nextFloat() < PERCENT_OF_ROOMS_WITHOUT_ENEMIES) continue
            val success = areaBuilder.addAtEmptyPosition(Rat(currentLevel), room.position, room.size)
            if (success) currNumEnemies += 1
        }

        while (currNumEnemies < maxNumEnemies)
        {
            // Place enemies in corridors
            if (Random.nextFloat() < CORR_ENEMY_CHANCE)
            {
                val path = allCorridors.random().path
                areaBuilder.addEntity(Rat(currentLevel), path.subList(1, path.size - 2).random())
            }

            // Place enemies in rooms based on rough distance (order of generation) and room size
            val room = allRooms.random()
            val roomArea = room.size.xLength * room.size.yLength
            if (Random.nextInt(MIN_ROOM_SIZE.xLength * MIN_ROOM_SIZE.yLength, MAX_ROOM_SIZE.xLength * MAX_ROOM_SIZE.yLength) > roomArea) continue
            if (Random.nextInt(0, allRooms.size) > allRooms.indexOf(room)) continue

            // Select an enemy to place
            val enemyToPlace : Enemy
            if (Random.nextFloat() < min(MAX_SNAKE_CHANCE, START_SNAKE_CHANCE + ADD_SNAKE_CHANCE * currentLevel))
            {
                enemyToPlace = Snake(currentLevel)
            }
            else
            {
                enemyToPlace = Rat(currentLevel)
            }

            val success = areaBuilder.addAtEmptyPosition(enemyToPlace, room.position, room.size)
            if (success) currNumEnemies += 1

            if (currNumEnemies >= maxNumEnemies) break

        }
    }


    /**
     * Places pots in all given rooms based on room size and distance from center.
     */
    private fun placePots(areaBuilder: AreaBuilder, allRooms: MutableList<Room>)
    {
        for (room in allRooms)
        {
            // Precalculate room mid-point and max distance
            val roomMidPoint = room.position + Position3D.create(room.size.xLength/2, room.size.yLength/2,0)
            val roomMaxDistFromMid = (room.size.xLength + room.size.yLength)/2

            // Generate a number of pots based on randomness and room area
            val numOfPots = min(Random.nextInt(MIN_POTS_IN_ROOM, MAX_POTS_IN_ROOM),(room.size.xLength * room.size.yLength)/10)
            var numPlacedPots = 0
            while (numPlacedPots < numOfPots)
            {
                // Generate pot position
                var potPosition = room.position + Position3D.create(Random.nextInt(0, room.size.xLength),
                                                                    Random.nextInt(0, room.size.yLength), 0)

                // Clamp the position to window bounds
                if (potPosition.x < 1) potPosition = potPosition.withX(1)
                if (potPosition.x > areaBuilder.width - 1) potPosition = potPosition.withX(areaBuilder.width - 1)
                if (potPosition.y < 1) potPosition = potPosition.withY(1)
                if (potPosition.y > areaBuilder.height - 1) potPosition = potPosition.withY(areaBuilder.height - 1)

                // Place pot only on floors
                if (areaBuilder.blocks[potPosition] is Wall) continue

                // Prefer edges of the room
                val potDistFromMid = (potPosition - roomMidPoint).chebyshevDistance
                if (Random.nextInt(0, roomMaxDistFromMid) > potDistFromMid) continue

                // Add pot
                areaBuilder.addEntity(Pot(), potPosition)
                numPlacedPots += 1
            }
        }
    }

    
    /**
     * Moving down - goes to a brand-new level.
     */
    override fun moveDown() {
        ++currentLevel
        this.logMessage("Descended to level ${currentLevel + 1}")
        if (currentLevel >= areas.size) areas.add(buildLevel())
        goToArea(areas[currentLevel])
    }


    /**
     * Moving up would be for revisiting past levels, we do not need that. Check [DungeonWorld] for an implementation.
     */
    override fun moveUp() {
        // Not implemented
    }

    data class Room(var isValid : Boolean, var position : Position3D, var size: Size3D)
    data class Corridor(var isValid : Boolean, var path : List<Position3D>)
}