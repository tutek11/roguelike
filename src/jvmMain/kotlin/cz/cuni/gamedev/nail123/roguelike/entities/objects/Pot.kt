package cz.cuni.gamedev.nail123.roguelike.entities.objects

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.SortingLayer
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Interactable
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.InteractionType
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.interactionContext
import cz.cuni.gamedev.nail123.roguelike.entities.items.HPPotion
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import java.awt.dnd.DropTarget

class Pot(): GameEntity(GameTiles.POT), Interactable {

    override val blocksMovement = true
    override val blocksVision = false

    val drops = listOf(
        5 to dropTypes.nothing,
        2 to dropTypes.potion)

    enum class dropTypes {
        nothing,
        potion,
    }

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<Player>(InteractionType.BUMPED) {
            area.removeEntity(this@Pot)
            val drop = drops.random()
            if (drop.second == dropTypes.potion) area.addEntity(HPPotion(), position)
        }
    }
}