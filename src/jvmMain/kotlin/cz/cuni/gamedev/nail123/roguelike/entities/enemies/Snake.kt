package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.blocks.Wall
import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasSmell
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.InteractionType
import cz.cuni.gamedev.nail123.roguelike.entities.items.HPPotion
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.extensions.chebyshevDistance
import cz.cuni.gamedev.nail123.roguelike.mechanics.Navigation
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.mechanics.goBlindlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import cz.cuni.gamedev.nail123.roguelike.world.Direction
import org.hexworks.zircon.api.data.Position3D
import kotlin.random.Random

class Snake(private val currentLevel: Int): Enemy(GameTiles.SNAKE), HasSmell {
    override val blocksMovement = true
    override val blocksVision = false
    override val smellingRadius = 4

    override val maxHitpoints = 16 + (currentLevel * 2f).toInt()
    override var hitpoints = 16 + (currentLevel * 2f).toInt()
    override var attack = 1
    override var defense = 0

    val drops = listOf(
        5 to dropTypes.nothing,
        1 to dropTypes.sword)

    enum class dropTypes {
        nothing,
        sword
    }

    val allDirections = arrayOf(
        Position3D.create(1,0,0),
        Position3D.create(0,1,0),
        Position3D.create(1,1,0),
        Position3D.create(-1,0,0),
        Position3D.create(0,-1,0),
        Position3D.create(-1,-1,0),
        Position3D.create(1,-1,0),
        Position3D.create(-1,1,0),
        )

    var isRunningAway = false
    var tilesRan = 0

    override fun update() {
        if (isRunningAway)
        {
            // Find the direction futherest from the player
            var bestDir = Position3D.create(0,0,0)
            var bestDist = 0
            for (direction in allDirections)
            {
                if (area.blocks[position + direction] is Wall) continue
                if (area.player.position == position + direction) continue

                val currDist = Pathfinding.manhattan(position + direction, area.player.position)
                if (currDist > bestDist)
                {
                    bestDist = currDist
                    bestDir = direction
                }
            }
            move(Direction.fromPosition(bestDir)!!)
            tilesRan += 1
        }
        else if (Pathfinding.chebyshev(position, area.player.position) <= smellingRadius) {
            goBlindlyTowards(area.player.position)
        }

        if (tilesRan == 5)
        {
            tilesRan = 0
            isRunningAway = false
        }
    }

    override fun interactWith(other: GameEntity, type: InteractionType): Boolean {
        if (other is Player){
            isRunningAway = true
        }
        return super.interactWith(other, type)
    }

    override fun die() {
        super.die()
        // Drop an item perhaps
        val drop = drops.random()
        if (drop.second == dropTypes.sword)
            this.block.entities.add(Sword(Random.nextInt(2, 5) + currentLevel *2))
    }
}