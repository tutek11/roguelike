package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Inventory
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class HPPotion(): Item(GameTiles.POTION) {
    override fun isEquipable(character: HasInventory): Inventory.EquipResult {
        return Inventory.EquipResult.Success
    }

    override fun onEquip(character: HasInventory) {
        if (character is Player) {
            character.hitpoints = character.maxHitpoints
            character.inventory.remove(this)
        }
    }

    override fun onUnequip(character: HasInventory) {
    }

    override fun toString(): String {
        return "HP Potion"
    }
}