package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasSmell
import cz.cuni.gamedev.nail123.roguelike.entities.items.HPPotion
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.mechanics.goBlindlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import kotlin.random.Random

class Rat(private val currentLevel: Int): Enemy(GameTiles.RAT), HasSmell {
    override val blocksMovement = true
    override val blocksVision = false
    override val smellingRadius = 4

    override val maxHitpoints = 12 + (currentLevel * 2f).toInt()
    override var hitpoints = 12 + (currentLevel * 2f).toInt()
    override var attack = 3
    override var defense = 0

    val drops = listOf(
        5 to dropTypes.nothing,
        1 to dropTypes.potion,
        2 to dropTypes.sword)

    enum class dropTypes {
        nothing,
        potion,
        sword
    }


    override fun update() {
        if (Pathfinding.chebyshev(position, area.player.position) <= smellingRadius) {
            goBlindlyTowards(area.player.position)
        }
    }

    override fun die() {
        super.die()
        // Drop an item perhaps
        val drop = drops.random()
        if (drop.second == dropTypes.potion)
            this.block.entities.add(HPPotion())
        else if (drop.second == dropTypes.sword)
            this.block.entities.add(Sword(Random.nextInt(2, 5) + currentLevel *2))
    }
}